<?xml version='1.0'?>
<!--

    Copyright 2018-2024 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="recognition"
  base-template="standard"
  provide-type="standard-type"
  context-provider="de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocTimedTitleContextProvider">
  <resource-bundle>
    <l10n>
      <name>Recognition</name>
      <description>
        Provide recognition!
      </description>
      <about>
        Provide a link to an information you want to increase recognition.
        State why this result stands out and should be recognized.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Anerkennungen">Anerkennung</name>
      <description>
        Kennen Sie ein Arbeitsergebnis an!
      </description>
      <about>
        Kennen Sie das Arbeitsergebnis an, auf das Sie per Link verweisen.
        Stellen Sie heraus, warum das Ergebnis sich von anderen Ergebnissen
        absetzt.
      </about>
      <type plural="Anerkennungstypen">Anerkennungstyp</type>
    </l10n>
  </resource-bundle>

  <metadata>
    <property key="projectdoc.doctype.common.is-a">record</property>
  </metadata>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">recognition-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.recognition.author">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder</param>
          <param
            name="property"
            key="projectdoc.doctype.recognition.author" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Author</name>
        </l10n>
        <l10n locale="de">
          <name>Autor</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.recognition.target">
      <value>
        <macro name="projectdoc-transclusion-property-display">
          <param name="add-link">true</param>
          <param name="property-name" key="projectdoc.doctype.common.name" />
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Target</name>
        </l10n>
        <l10n locale="de">
          <name>Gegenstand</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.date">
      <value><xml><![CDATA[<at:var at:name="day-date-picker" at:rawxhtml="true"/>]]></xml></value>
      <controls>hide</controls>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            In case the short description is not enough, the description section
            provides room for more detailed information for the recognition.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Sollte die Kurzbeschreibung nicht genügen, die Anerkennung
            ausreichend zu beschreiben, bietet der Beschreibungsabschnitt mehr
            Raum, um detaillierte Informationen für Ihr Anerkennung bereit zu
            stellen.
          </description>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="conversation" />
    <doctype-ref id="feedback" />
  </related-doctypes>

  <wizard template="minimal-fields" form-id="recognition-form">
    <field template="date" name="day-date" key="projectdoc.doctype.common.date"/>
    <field template="target-location" />
    <param name="projectdoc-adjustVarValues-toDatePicker">day-date</param>
  </wizard>
</doctype>
