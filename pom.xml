<?xml version="1.0" encoding="UTF-8"?>

<!--suppress UnresolvedMavenProperty -->
<project
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
    xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <modelVersion>4.0.0</modelVersion>
  <groupId>de.smartics.atlassian.confluence</groupId>
  <artifactId>smartics-doctype-model-cfrs</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <name>Conversation, Feedback, and Recognition Model</name>
  <description>Models for: Together with OKRs CFRs are capable to lift your
    continuous performance management to the next level. This add-on provides
    blueprints to run CFRs with Confluence and the projectdoc Toolbox.
  </description>
  <inceptionYear>2018</inceptionYear>
  <organization>
    <name>smartics</name>
    <url>http://www.smartics.de/</url>
  </organization>
  <licenses>
    <license>
      <name>The Apache Software License, Version 2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0</url>
      <distribution>repo</distribution>
      <comments>Copyright 2018-2024 Kronseder &amp; Reiner GmbH, smartics
      </comments>
    </license>
  </licenses>
  <developers>
    <developer>
      <id>robert.reiner</id>
      <name>Robert Reiner</name>
      <url>https://www.smartics.de/go/robertreiner</url>
      <organization>Kronseder &amp; Reiner GmbH, smartics</organization>
      <organizationUrl>http://www.smartics.de/</organizationUrl>
    </developer>
    <developer>
      <id>anton.kronseder</id>
      <name>Anton Kronseder</name>
      <url>https://www.smartics.de/go/antonkronseder</url>
      <organization>Kronseder &amp; Reiner GmbH, smartics</organization>
      <organizationUrl>http://www.smartics.de/</organizationUrl>
    </developer>
  </developers>
  <prerequisites>
    <maven>3.0.3</maven>
  </prerequisites>
  <scm>
    <connection>scm:git:https://bitbucket.org/smartics/${project.artifactId}
    </connection>
    <developerConnection>
      scm:git:https://bitbucket.org/smartics/${project.artifactId}
    </developerConnection>
    <url>https://bitbucket.org/smartics/${project.artifactId}</url>
  </scm>
  <issueManagement>
    <system>jira</system>
    <url>https://www.smartics.eu/jira/projects/CFRS</url>
  </issueManagement>
  <distributionManagement>
    <repository>
      <id>public</id>
      <name>internal smartics release repository</name>
      <url>dav:https://www.smartics.eu/nexus/content/repositories/public</url>
    </repository>
    <snapshotRepository>
      <uniqueVersion>false</uniqueVersion>
      <id>public-snapshot</id>
      <name>internal smartics snapshot repository</name>
      <url>
        dav:https://www.smartics.eu/nexus/content/repositories/public-snapshot
      </url>
    </snapshotRepository>
  </distributionManagement>
  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

    <apptools-maven-plugin.version>0.12.1</apptools-maven-plugin.version>
    <doctype-maven-plugin.version>3.2.0-SNAPSHOT</doctype-maven-plugin.version>
    <smartics-projectdoc-confluence.version>6.2.4-SNAPSHOT</smartics-projectdoc-confluence.version>
    <smartics-projectdoc-confluence.version-space-core>18.0.1-SNAPSHOT</smartics-projectdoc-confluence.version-space-core>

    <buildmetadata-maven-plugin.version>1.7.0</buildmetadata-maven-plugin.version>

    <maven-license-plugin.version>1.9.0</maven-license-plugin.version>
    <maven-clean-plugin.version>2.6.1</maven-clean-plugin.version>
    <maven-site-plugin.version>3.4</maven-site-plugin.version>
    <maven-release-plugin.version>2.5.3</maven-release-plugin.version>
    <maven-deploy-plugin.version>2.8.2</maven-deploy-plugin.version>
    <maven-jar-plugin.version>2.6</maven-jar-plugin.version>
    <maven-install-plugin.version>2.5.2</maven-install-plugin.version>
    <maven-resources-plugin.version>2.7</maven-resources-plugin.version>
    <maven-scm-plugin.version>1.9.4</maven-scm-plugin.version>
    <maven-dependency-plugin.version>2.10</maven-dependency-plugin.version>
  </properties>
  <build>
    <extensions>
      <extension>
        <groupId>org.apache.maven.wagon</groupId>
        <artifactId>wagon-webdav</artifactId>
        <version>1.0-beta-2</version>
      </extension>
    </extensions>
    <pluginManagement>
      <plugins>
        <plugin>
          <artifactId>maven-clean-plugin</artifactId>
          <version>${maven-clean-plugin.version}</version>
        </plugin>
        <plugin>
          <artifactId>maven-dependency-plugin</artifactId>
          <version>${maven-dependency-plugin.version}</version>
        </plugin>
        <plugin>
          <artifactId>maven-resources-plugin</artifactId>
          <version>${maven-resources-plugin.version}</version>
        </plugin>
        <plugin>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>${maven-deploy-plugin.version}</version>
        </plugin>
        <plugin>
          <artifactId>maven-jar-plugin</artifactId>
          <version>${maven-jar-plugin.version}</version>
          <configuration>
            <archive>
              <index>true</index>
              <manifest>
                <addDefaultImplementationEntries>true
                </addDefaultImplementationEntries>
                <addDefaultSpecificationEntries>true
                </addDefaultSpecificationEntries>
              </manifest>
              <manifestEntries>
                <Implementation-URL>${project.url}</Implementation-URL>
                <Built-OS>${os.name} / ${os.arch} / ${os.version}</Built-OS>
                <Maven-Version>${build.maven.version}</Maven-Version>
                <Java-Version>${java.version}</Java-Version>
                <Java-Vendor>${java.vendor}</Java-Vendor>
              </manifestEntries>
            </archive>
          </configuration>
        </plugin>
        <plugin>
          <artifactId>maven-install-plugin</artifactId>
          <version>${maven-install-plugin.version}</version>
        </plugin>
        <plugin>
          <artifactId>maven-release-plugin</artifactId>
          <version>${maven-release-plugin.version}</version>
          <configuration>
            <useReleaseProfile>false</useReleaseProfile>
            <arguments>-P release-doctype-add-on</arguments>
          </configuration>
        </plugin>

        <plugin>
          <artifactId>maven-scm-plugin</artifactId>
          <version>${maven-scm-plugin.version}</version>
          <configuration>
            <connectionType>developerConnection</connectionType>
          </configuration>
        </plugin>

        <!--
        <plugin>
          <groupId>org.eclipse.m2e</groupId>
          <artifactId>lifecycle-mapping</artifactId>
          <version>1.0.0</version>
          <configuration>
            <lifecycleMappingMetadata>
              <pluginExecutions>
                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>de.smartics.maven.plugin</groupId>
                    <artifactId>buildmetadata-maven-plugin</artifactId>
                    <versionRange>[0.0.0,)</versionRange>
                    <goals>
                      <goal>provide-buildmetadata</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore/>
                  </action>
                </pluginExecution>
                <pluginExecution>
                  <pluginExecutionFilter>
                    <groupId>de.smartics.maven.plugin</groupId>
                    <artifactId>doctype-maven-plugin</artifactId>
                    <versionRange>[0.0.0,)</versionRange>
                    <goals>
                      <goal>create</goal>
                      <goal>generate</goal>
                    </goals>
                  </pluginExecutionFilter>
                  <action>
                    <ignore></ignore>
                  </action>
                </pluginExecution>
              </pluginExecutions>
            </lifecycleMappingMetadata>
          </configuration>
        </plugin> -->

        <plugin>
          <!-- Usage: mvn apptools:deploy -PLOCAL -->
          <groupId>de.smartics.maven.plugin</groupId>
          <artifactId>apptools-maven-plugin</artifactId>
          <version>${apptools-maven-plugin.version}</version>
          <configuration>
            <sourceFolder>${basedir}/target/smartics-doctype-addon-cfrs/target
            </sourceFolder>
            <includes>
              <include>smartics-doctype-addon-cfrs$</include>
            </includes>
            <acceptedFilenameExtensions>
              <extension>jar</extension>
            </acceptedFilenameExtensions>

            <serverId>${projectdoc.apptools.remote.server.id}</serverId>
            <serverUrl>${projectdoc.apptools.remote.server.url}</serverUrl>
          </configuration>
        </plugin>
      </plugins>
    </pluginManagement>
    <plugins>
      <plugin>
        <groupId>de.smartics.maven.plugin</groupId>
        <artifactId>buildmetadata-maven-plugin</artifactId>
        <version>${buildmetadata-maven-plugin.version}</version>
        <executions>
          <execution>
            <phase>initialize</phase>
            <goals>
              <goal>provide-buildmetadata</goal>
            </goals>
          </execution>
        </executions>
       <configuration>
          <buildDatePattern>dd.MM.yyyy HH:mm:ss</buildDatePattern>
          <addScmInfo>false</addScmInfo>
        </configuration>
      </plugin>
      <plugin>
        <groupId>com.mycila.maven-license-plugin</groupId>
        <artifactId>maven-license-plugin</artifactId>
        <version>${maven-license-plugin.version}</version>
        <executions>
          <execution>
            <goals>
              <goal>check</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <strictCheck>true</strictCheck>
          <header>src/etc/license/header.txt</header>
          <headerDefinitions>
            <headerDefinition>src/etc/license/javadoc.xml</headerDefinition>
          </headerDefinitions>
          <properties>
            <year>${build.copyright.year}</year>
          </properties>
          <excludes>
            <exclude>**/COPYRIGHT.txt</exclude>
            <exclude>**/LICENSE</exclude>
            <exclude>**/LICENSE.txt</exclude>
            <exclude>**/LICENSE-*</exclude>
            <exclude>**/javadoc.xml</exclude>
            <exclude>**/header.txt</exclude>
            <exclude>**/pom.xml.releaseBackup</exclude>
            <exclude>.gitignore</exclude>
            <exclude>**/*.md</exclude>
            <exclude>**/*.soy</exclude>
            <exclude>src/main/resources/projectdoc-models/patch/**</exclude>
            <exclude>src/main/resources/projectdoc-models/tools/*/*.*</exclude>
            <exclude>**/.idea/**</exclude>
          </excludes>
          <mapping>
            <vm>XML_STYLE</vm>
          </mapping>
        </configuration>
      </plugin>
      <plugin>
        <artifactId>maven-resources-plugin</artifactId>
        <executions>
          <execution>
            <id>copy-resources</id>
            <phase>compile</phase>
            <goals>
              <goal>copy-resources</goal>
            </goals>
            <configuration>
              <outputDirectory>${project.build.outputDirectory}/META-INF
              </outputDirectory>
              <resources>
                <resource>
                  <directory>${basedir}</directory>
                  <includes>
                    <include>COPYRIGHT.txt</include>
                    <include>LICENSE-*</include>
                  </includes>
                  <filtering>false</filtering>
                </resource>
              </resources>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>de.smartics.maven.plugin</groupId>
        <artifactId>doctype-maven-plugin</artifactId>
        <version>${doctype-maven-plugin.version}</version>
        <executions>
          <execution>
            <id>generate-doctype</id>
            <phase>generate-resources</phase>
            <goals>
              <goal>create</goal>
              <goal>generate</goal>
            </goals>
          </execution>
          <execution>
            <id>build-obr</id>
            <phase>package</phase>
            <goals>
              <goal>build</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <readProjectSettings>false</readProjectSettings>
          <verbose>true</verbose>

          <shortId>cfrs</shortId>
          <projectName>Conversation, Feedback, and Recognition</projectName>
          <projectDescription>Together with OKRs CFRs are capable to lift your
            continuous performance management to the next level. This add-on
            provides blueprints to run CFRs with Confluence and the projectdoc
            Toolbox.
          </projectDescription>
          <organizationSignature>Kronseder &amp; Reiner GmbH, smartics
          </organizationSignature>
          <createExamples>false</createExamples>

          <packagePrefix>de.smartics.projectdoc</packagePrefix>
          <modelsFolder>/src/main/resources/projectdoc-models</modelsFolder>
          <groupId>de.smartics.atlassian.confluence</groupId>
          <artifactIdPrefix>smartics-doctype-addon-</artifactIdPrefix>
          <projectVersion>${project.version}</projectVersion>
          <mainSpaceId>main</mainSpaceId>

          <mainSpaceId>main</mainSpaceId>

          <confluenceCompatibilityMin>7.19.24</confluenceCompatibilityMin>
          <confluenceCompatibilityMax>9.0.1</confluenceCompatibilityMax>

          <pluginArtifacts>
            <pluginArtifact>
              <groupId>de.smartics.atlassian.confluence</groupId>
              <artifactId>smartics-projectdoc-confluence-space-core</artifactId>
              <version>${smartics-projectdoc-confluence.version-space-core}
              </version>
            </pluginArtifact>
          </pluginArtifacts>

          <references>
            <reference>
              <name>addon-documentation</name>
              <type>confluence</type>
              <locator>http://www.smartics.eu/confluence/display/PDAC1</locator>
            </reference>
          </references>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <profiles>
    <profile>
      <id>release-doctype-add-on</id>
      <properties>
        <project.atlassian.plugin.version>${project.version}
        </project.atlassian.plugin.version>
      </properties>
    </profile>
  </profiles>
</project>
