<?xml version='1.0'?>
<!--

    Copyright 2018-2024 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="conversation"
  base-template="standard"
  provide-type="standard-type"
  context-provider="de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocTimedTitleContextProvider">
  <resource-bundle>
    <l10n>
      <name>Conversation</name>
      <description>
        Briefly document the results of a conversation.
      </description>
      <about>
        Document the central points of a conversation that is important at a
        later time. In contrast to meeting minutes the conversation is intended
        as a conversation between stakeholders, often 1-on-1.
      </about>
    </l10n>
    <l10n locale="de">
      <name>Konversation</name>
      <description>
        Dokumentieren Sie eine Konversation stichpunktartig.
      </description>
      <about>
        Dokumentieren Sie die zentralen Ergebnisse einer Konversation, um
        später darauf zurückzugreifen. Im Gegensatz zu einem Protokoll hält
        ein Konversationsdokument Informationen zu einem Mitarbeitergespräch
        zusammen, das häufig zwischen zwei Stakeholdern geführt wird.
      </about>
      <type plural="Konversationstypen">Konversationstyp</type>
    </l10n>
  </resource-bundle>

  <metadata>
    <property key="projectdoc.doctype.common.is-a">record</property>
  </metadata>

  <properties>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">conversation-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="property-restrict-value-range">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.conversation.author">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder</param>
          <param
            name="property"
            key="projectdoc.doctype.conversation.author" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Author</name>
        </l10n>
        <l10n locale="de">
          <name>Autor</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.participants">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder</param>
          <param
            name="property"
            key="projectdoc.doctype.common.participants" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.conversation.target">
      <value>
        <macro name="projectdoc-transclusion-property-display">
          <param name="add-link">true</param>
          <param name="property-name" key="projectdoc.doctype.common.name" />
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Target</name>
        </l10n>
        <l10n locale="de">
          <name>Gegenstand</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.date">
      <value><xml><![CDATA[<at:var at:name="day-date-picker" at:rawxhtml="true"/>]]></xml></value>
      <controls>hide</controls>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            In case the short description is not enough, the description section
            provides room for more detailed information about the context of
            the conversation.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Sollte die Kurzbeschreibung nicht genügen, dann bietet der
            Beschreibungsabschnitt mehr Raum, um detaillierte Informationen zum
            Kontext der Konversation bereitzustellen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.conversation.progress-update">
      <resource-bundle>
        <l10n>
          <name>Progress Update</name>
          <description>
            Document what happened since the last conversation. Focus on the
            situation at work. Focus on work results later. What is working
            well, where is room for improvement, who could help with some
            situation?
          </description>
        </l10n>
        <l10n locale="de">
          <name>Aktuelle Lage</name>
          <description>
            Dokumentieren Sie, was seit der letzten Konversation passiert ist.
            Fokussieren Sie sich auf die Arbeitssitution insgesamt und gehen
            Sie auf konkrete Arbeitsergebnisse gesondert ein. Was läuft gut, wo
            gibt es Verbesserungsmöglichkeiten, wer kann in einer bestimmten
            Situation vielleicht weiterhelfen?
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.conversation.work-results">
      <resource-bundle>
        <l10n>
          <name>Work Results</name>
          <description>
            Document the results of the work. How do the results relate with
            the goals? have the objectives been met? Is help required and how
            could provide this help?
          </description>
        </l10n>
        <l10n locale="de">
          <name>Arbeitsergebnisse</name>
          <description>
            Dokumentieren Sie welche Ergebnisse erarbeitet wurden. Stimmen die
            Ergebnisse mit den Zielen überein? Wurden die Ziele erreicht? Ist
            Hilfe notwendig und wer könnte diese Hilfe leisten?
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.conversation.reflection">
      <resource-bundle>
        <l10n>
          <name>Retrospective</name>
          <description>
            Document reflection results regarding the work. What has been done
            what turned out to be working pretty well, where is room for
            improvement in the future, who could help to get the work more
            efficiently or effectively done?
          </description>
        </l10n>
        <l10n locale="de">
          <name>Retrospektive</name>
          <description>
            Dokumentieren Sie der Ergebnisse der Selbstreflexion im Kontext der
            geleisteten Arbeit. Was hat sich als erfolgreich herausgestellt,
            wo ist Raum für Verbesserung in der Zukunft, wer kann dabei helfen
            bestimmte Arbeiten effizienter oder effektiver durchzuführen?
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.conversation.work-planning">
      <resource-bundle>
        <l10n>
          <name>Planned Work</name>
          <description>
            Document what is planned as immediate next steps. What are the
            objectives? Are the objectives related to the company's strategy
            and objectives? Which resources are required? In which time will
            the objectives been met? How is the weekly plan to achieve those
            objectives?
          </description>
        </l10n>
        <l10n locale="de">
          <name>Geplante Arbeiten</name>
          <description>
            Dokumentieren Sie die nächsten unmittelbaren Ziele für die Arbeit.
            Welche neuen Ziele werden geplant? Wie stimmen die Ziele mit der
            Firmenstrategie und den Firmenzielen überein? Welche Ressourcen
            sind notwendig? In welchem Zeitrahmen werden die einzelnen Ziele
            erarbeitet? Wie ist die Planung der Woche(n) auf die Ziele?
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.conversation.personal-objectives">
      <resource-bundle>
        <l10n>
          <name>Personal Objectives</name>
          <description>
            Document the personal objectives of the team member's career.
            How can the company support the team member to meet those goals?
          </description>
        </l10n>
        <l10n locale="de">
          <name>Berufliche Ziele</name>
          <description>
            Dokumentieren Sie die beruflichen Ziele des Mitarbeiters. Wie kann
            die Firma den Mitarbeiter bei der Erreichung dieser Ziele
            unterstützen?
          </description>
        </l10n>
      </resource-bundle>
    </section>

  </sections>

  <related-doctypes>
    <doctype-ref id="feedback" />
    <doctype-ref id="recognition" />
  </related-doctypes>

  <wizard template="minimal-fields" form-id="conversation-form">
    <field template="date" name="day-date" key="projectdoc.doctype.common.date"/>
    <field template="target-location" />
    <param name="projectdoc-adjustVarValues-toDatePicker">day-date</param>
  </wizard>
</doctype>
